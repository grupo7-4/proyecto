//Configuracion firebase

  // Import the functions you need from the SDKs you need

  import { initializeApp } 
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

  // TODO: Add SDKs for Firebase products that you want to use
  import{ getDatabase,onValue,ref,set,get,child,update,remove}
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

  import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
  import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
  // https://firebase.google.com/docs/web/setup#available-libraries


  // Your web app's Firebase configuration

  const firebaseConfig = {

    apiKey: "AIzaSyBxvt-7ACDheoIJwomyo3Ti-3SBdWXKhf8",
    authDomain: "proyectoweb-74jcml.firebaseapp.com",
    databaseURL: "https://proyectoweb-74jcml-default-rtdb.firebaseio.com",
    projectId: "proyectoweb-74jcml",
    storageBucket: "proyectoweb-74jcml.appspot.com",
    messagingSenderId: "785248299310",
    appId: "1:785248299310:web:0191a53ccf98974712b2bf"

  };


  // Initialize Firebase

  const app = initializeApp(firebaseConfig);
  const db = getDatabase();
  const auth = getAuth();
  //storage
  

  //declaracion de objetos
  var btnInsertar = document.getElementById("btnInsertar");
  var btnBuscar = document.getElementById("btnBuscar");
  var btnActualizar = document.getElementById("btnActualizar");
  var btnBorrar = document.getElementById("btnBorrar");
  var btnTodos = document.getElementById("btnTodos");
  var verImagen = document.getElementById('verImagen')
  var btnLimpiar = document.getElementById('btnLimpiar')


  var lista = document.getElementById("lista");
  var archivo = document.getElementById('archivo');
  var todo = document.getElementById('todo');
  var id="";
  var nombre="";
  var descripcion="";
  var precio="";
  var url="";
  var imgNombre="";
  var disponible="";
  var imagen = document.getElementById('imagen')

  function leerinputs(){

    id = document.getElementById('id').value
    nombre = document.getElementById('nombre').value
    descripcion = document.getElementById('descripcion').value
    precio = document.getElementById('precio').value
    url = document.getElementById('url').value
    imgNombre = document.getElementById('imgNombre').value
    disponible = document.getElementById('disponible').value

    //alert( "Matricula: " + matricula+" Nombre :"+nombre+" Carrera: "+carrera+" Genero: "+genero)

    
  }

  function insertarDatos(){
    leerinputs();

    set(ref(db,'productos/'+id),{
      nombre:nombre,
      descripcion:descripcion,
      precio:precio,
      url:url,
      imgNombre:imgNombre,
      disponible:disponible
   

    }).then((response)=>{
      alert(
      "Se agrego el registro con exito")
    })
    .catch((error)=>{
      alert("Surgio un error"+error)
      
    });
  }

  function mostrarDatos(){
    leerinputs();

    const dbref = ref(db);

    get(child(dbref,'productos/'+id)).then((snapshot)=>{
      if(snapshot.exists()){
        nombre = snapshot.val().nombre;
        descripcion = snapshot.val().descripcion;
        precio = snapshot.val().precio;
        url = snapshot.val().url;
        imgNombre = snapshot.val().imgNombre;
        disponible = snapshot.val().disponible;

        escribirInputs();
      }else{
        alert("No existe la matricula");

      }
    }).catch((error)=>{
      alert("Surgio un error"+ error)
    })
  }

  function escribirInputs(){
   
    id = document.getElementById('id').value=id;
    nombre = document.getElementById('nombre').value=nombre;
    descripcion = document.getElementById('descripcion').value=descripcion;
    precio = document.getElementById('precio').value=precio;
    url = document.getElementById('url').value=url;
    imgNombre = document.getElementById('imgNombre').value=imgNombre;
    disponible = document.getElementById('disponible').value=disponible;

  }


/*
  function mostrarProductos(){
    const db = getDatabase();
    const dbRef = ref(db, 'productos');
    onValue(dbRef, (snapshot) => {
     lista.innerHTML=""
     snapshot.forEach((childSnapshot) => {
     const childKey = childSnapshot.key;
     const childData = childSnapshot.val();
     
     lista.innerHTML = "<div class='campo'> " + lista.innerHTML + childKey +"\n" +
    childData.nombre+"\n" +childData.precio +"\n" +childData.disponible+"<br> </div>";
     console.log(childKey + ":");
     console.log(childData.nombre)
     // ...
     });
    }, {
     onlyOnce: true
    });
    }
*/


if(window.location.href == "https://practicaswebjcml117.000webhostapp.com/Proyecto/html/login.html"){
    window.onload = mostrarProductos();    
}


if(window.location.href == "https://2020030810.000webhostapp.com/html/admin.html"){
    window.onload = mostrarProductos(); 
}

if(window.location.href == "http://127.0.0.1:5500/html/p002_productos.html"){
    window.onload = mostrarProductos(); 
    
}


function mostrarProductos(){
  
  const db = getDatabase();
  const dbref = ref(db, 'productos');
  onValue(dbref, (snapshot)=>{
      snapshot.forEach(childSnapshot => {
      const childKey = childSnapshot.key;
      const childData = childSnapshot.val();
      if(lista){
          if(childData.disponible == 0){
              lista.innerHTML = lista.innerHTML + "<div class='productos'><div class='foto'><img src='"+childData.url+"' style='height: 230px; width: 250px;'></img><h4>ID:"+childKey+"DADO DE BAJA "
              +childData.nombre+"</h4><p style='text-align: center;'>"+childData.precio+"</p><div class='descripcion'><p>"
              +childData.descripcion+"</p></div></div></div>"
          }
          if(childData.disponible == 1){
              lista.innerHTML = lista.innerHTML + "<div class='productos'><div class='foto'><img src='"+childData.url+"' style='height: 230px; width: 250px;'></img><h4>ID:"+childKey+" "
              +childData.nombre+"</h4><p style='text-align: center;'>"+childData.precio+"</p><div class='descripcion'><p>"
              +childData.descripcion+"</p></div></div></div>"
            }
      }
      if(todo){
         if(childData.disponible == 1){
              todo.innerHTML = todo.innerHTML + "<div class='productos'><div class='foto'><img src='"+childData.url+"' style='height: 230px; width: 250px;'></img><h4>"+
              childData.nombre+"</h4><p style='text-align: center;'>"+childData.precio+"</p><div class='descripcion'><p>"
              +childData.descripcion+"</p></div></div></div>"
    }
  }
                                            
      });
  },{
      onlyOnce: true
  });

}


function imprimir(){
  mostrarProductos();
  limpiar
}

  function actualizar(){
      leerinputs();
     update(ref(db,'productos/'+ id),{
      nombre:nombre,
      descripcion:descripcion,
      precio:precio,
      url:url,
      imgNombre:imgNombre,
      disponible:disponible
     }).then(()=>{
      alert("se realizo actualizacion");
      mostrarProductos();
     })
     .catch(()=>{
      alert("causo Error " + error );
  });
  limpiar();
  }

  function baja(){
    leerinputs();
   update(ref(db,'productos/'+ id),{
    nombre:nombre,
    descripcion:descripcion,
    precio:precio,
    url:url,
    imgNombre:imgNombre,
    disponible:"0"
   }).then(()=>{
    alert("se realizo actualizacion");
    limpiar
    mostrarProductos();
   })
   .catch(()=>{
    alert("causo Error " + error );
});
}





  /*
  function borrar(){
    leerinputs();
    remove(ref(db,'productos/'+ id)).then(()=>{
    alert("Se borro el registro");
    mostrarProductos();
    })
    .catch(()=>{
    alert("causo Error " + error );
    });
    
    
   }
*/





   function limpiar(){
    lista.innerHTML="";
   

    id="";
    nombre="";
    descripcion="";
    precio="";
    url="";
    imgNombre="";
    disponible="1";
    escribirInputs();
   }


function cargarImagen(){
  const file = event.target.files[0];
  const name = event.target.files[0].name;

  const storage = getStorage();
  const storageRef = refS(storage, 'imagenes/' + name);

  uploadBytes(storageRef,file).then((snapshot) => {
    document.getElementById('imgNombre').value=name;

    alert('Se cargo el archivo');
    
  });

}

function descargarImagen(){
  archivo=document.getElementById('imgNombre').value;
  
      
    // Create a reference to the file we want to download
    const storage = getStorage();
    const starsRef = refS(storage, 'imagenes/'+archivo);

    
    // Get the download URL
    getDownloadURL(starsRef)
      .then((url) => {
        document.getElementById('url').value=url;
        document.getElementById('imagen').src=url;
      })
      .catch((error) => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
          case 'storage/object-not-found':
            console.log("No existe el archivo")
            // File doesn't exist
            break;
          case 'storage/unauthorized':
            console.log("No tiene permisos")
            // User doesn't have permission to access the object
            break;
          case 'storage/canceled':
            console.log("Se ha cancelado la descarga")
            // User canceled the upload
            break;

          // ...

          case 'storage/unknown':
            console.log("Sucedio algo inesperado")
            // Unknown error occurred, inspect the server response
            break;
        }
      });
}


     //eventos on click
btnInsertar.addEventListener('click',insertarDatos);
btnBuscar.addEventListener('click',mostrarDatos);
btnActualizar.addEventListener('click',actualizar);
btnBorrar.addEventListener('click',baja);
btnTodos.addEventListener('click', imprimir);
btnLimpiar.addEventListener('click', limpiar);
archivo.addEventListener('change',cargarImagen)
verImagen.addEventListener('click',descargarImagen)