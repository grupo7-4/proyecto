import { initializeApp } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

// TODO: Add SDKs for Firebase products that you want to use
import{ getDatabase,onValue,ref,set,get,child,update,remove}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration

const firebaseConfig = {

  apiKey: "AIzaSyBxvt-7ACDheoIJwomyo3Ti-3SBdWXKhf8",
  authDomain: "proyectoweb-74jcml.firebaseapp.com",
  databaseURL: "https://proyectoweb-74jcml-default-rtdb.firebaseio.com",
  projectId: "proyectoweb-74jcml",
  storageBucket: "proyectoweb-74jcml.appspot.com",
  messagingSenderId: "785248299310",
  appId: "1:785248299310:web:0191a53ccf98974712b2bf"

};

const app = initializeApp(firebaseConfig);
const db = getDatabase();

const auth = getAuth();

var btnLimpiarLog = document.getElementById('btnLimpiarLog')
var btnIniciarLog = document.getElementById('btnIniciarLog')

var email="";
var password="";
var username="";


//login
function iniciarSesion(){
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
  
    createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
    // Signed in 
    const user = userCredential.user;
    alert('Usuario creado');
    // ...
    })
    .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
  
    alert('Algo salio mal');
    // ..
    });
  }
  
  function limpiarLogin(){
  
    email="";
    password="";
  
    var email = document.getElementById('email').value =email;
    var password = document.getElementById('password').value = password;
  }
  
  //Iniciar secion
  function iniciarUsuario(){
    email = document.getElementById('email').value;
    password = document.getElementById('password').value;

    signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in 
      const user = userCredential.user;

      const dt = new Date();
       update(ref(db, 'users/' + user.uid),{
        last_login: dt,
      })

       alert('Usuario logeado!');
       window.location.href = "/html/index.html";
      // ...
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      console.log(errorMessage)
      alert('Usuario o contraseña incorrectos');
});
}

btnIniciarLog.addEventListener('click', iniciarUsuario)
btnLimpiarLog.addEventListener('click', limpiarLogin)